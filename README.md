# G'mic mods

    This is a growing collection of wallpapers made from photos taken with a Nikon CoolPix L830, and filtered using G'mic.
    Some were filtered by the camera's internal filters first.
    The resolutions are either 3840x2160, or the camera's native resolution.

# **These wallpapers are for non-commercial use by anyone.**

>>> They neither require nor are eligible for attribution or remuneration.

>>> They are constrained by no licenses.

>>> Free means freedom from and freedom to.

>>> My freedom to share these requires of you the same.
